﻿using System;
using System.Collections.Generic;

namespace Sünnipäev
{
    class Program
    {
        static void Main(string[] args)
        {
            // nimekiri inimestest
            var peopleList = new List<Person> {
            new Person("Harjulane", new DateTime(2011, 1, 20)),
            new Person("Läänemaalane", new DateTime(1982, 1, 4)),
            new Person("Virulane", new DateTime(2009, 1, 3)),
            new Person("Võrukas", new DateTime(1983, 7, 23)),
            new Person("Mulk", new DateTime(2019, 2, 2)),
            new Person("Pontius Pilatus", new DateTime(1980, 1, 1)),
            new Person("Keegi", new DateTime(1910, 3, 2)),
            new Person("Keegi veel", new DateTime(1999, 12, 1)),
            new Person("Eestlane", new DateTime(2004, 5, 10)),
            new Person("Saarlane", new DateTime(2002, 10, 19)),
            new Person("Hiidlane", new DateTime(2019, 4, 6)),
            new Person("Pärnakas", new DateTime(2001, 3, 28)),
            new Person("Tartlane", new DateTime(1993, 5, 26)),
            new Person("Raplakas", new DateTime(1920, 1, 17)),
            new Person("Järvakas", new DateTime(2001, 7, 4)),
            new Person("Põlvakas", new DateTime(2002, 4, 6)),
            new Person("Linlane", new DateTime(1984, 1, 1)),
            new Person("Maalane", new DateTime(1973, 6, 1)),
            new Person("Kosmopoliit", new DateTime(2000, 9, 13)),
            new Person("Kosmonaut", new DateTime(1989, 1, 2)),
        };

            // loo sünnikuupäeva järgi sorditud hulk inimestest
            var people = new SortedSet<Person>(peopleList, new ByBirthday());

            // salvesta esimene sünnipäevalaps ja leia esimene inimene nimekirjast,
            // kelle sünnipäev on täna või peale tänast, lõpeta otsing ja kuva inimese andmed
            Person personHavingNextBirthday = getFirstElement(people);
            foreach (var person in people)
            {
                if (person.isBirthdayTodayorAfter(DateTime.Today))
                {
                    personHavingNextBirthday = person;
                    break;
                }
            }
            Console.WriteLine("Tere " + personHavingNextBirthday.ToString() + " sinu sünnipäev on järgmisena!");

            // arvuta inimeste keskmine vanus
            double sum_age = 0;
            foreach (var person in peopleList)
            {
                sum_age += person.Age;
                Console.WriteLine(person.ToString() + " " + person.Age + "-aastane");
            }
            Console.WriteLine("Inimeste keskmine vanus on:" + sum_age / peopleList.Count + " aastat");
        }
        static Person getFirstElement(SortedSet<Person> people)
        {
            IEnumerator<Person> peopleIterator = people.GetEnumerator();
            peopleIterator.MoveNext();
            return peopleIterator.Current;
        }
    }

}

public class Person
{
    public string Name;
    public int Age;
    public DateTime Birthday;

    public Person(string name, DateTime birthday)
    {
        this.Name = name;
        this.Birthday = birthday;
        this.Age = DateTime.Today.Year - birthday.Year;
    }

    // meetod, mis ütleb, kas inimese sünnipäev on antud päeval või pärast seda
    public bool isBirthdayTodayorAfter(DateTime datetime)
    {
        if (this.Birthday.Month > datetime.Month)
        {
            return true;
        }
        else if (this.Birthday.Month == datetime.Month && this.Birthday.Day >= datetime.Day)
        {
            return true;
        }
        return false;
    }

    public override string ToString()
    {
        return this.Name + " (" + this.Birthday.ToString("d", System.Globalization.CultureInfo.CreateSpecificCulture("de-DE")) + ")";
    }
}

// Klass, mis implementeerib võrdlusfunktsiooni inimeste järjestamiseks hulgas
public class ByBirthday : IComparer<Person>
{
    public int Compare(Person p1, Person p2)
    {
        if (p1.Birthday.Month == p2.Birthday.Month) {
            return p1.Birthday.Day - p2.Birthday.Day;
        }
        else
        {
            return p1.Birthday.Month - p2.Birthday.Month;
        }
    }
}